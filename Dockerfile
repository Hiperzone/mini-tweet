FROM frekele/gradle

ENV LANG C.UTF-8

USER root

ENV INSTALL_FOLDER=/opt/twitter

#create the destination folder
RUN mkdir -p $INSTALL_FOLDER
WORKDIR $INSTALL_FOLDER

#copy the distribution files
#-----------------rest-api BI server--------------------
COPY build/install/minitwitter $INSTALL_FOLDER/

#copy the resources
COPY build/resources/main $INSTALL_FOLDER/bin/src/main/resources

EXPOSE 8080

WORKDIR $INSTALL_FOLDER/bin/

#set permissions to run
RUN chmod -R 700 minitwitter

CMD ["./minitwitter"]