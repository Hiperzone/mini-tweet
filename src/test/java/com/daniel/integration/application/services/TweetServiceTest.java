package com.daniel.integration.application.services;

import com.daniel.application.services.TweetService;
import com.daniel.application.services.UserService;
import com.daniel.domain.model.tweet.Tweet;
import com.daniel.domain.model.user.User;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = com.daniel.context.MinitwitterApplication.class)
public class TweetServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private TweetService tweetService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void createTweet() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        Tweet tweet = tweetService.createTweet(user1.getUsername(), "@tweet this is a tweet");
        Validate.notNull(tweet);
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getTweetsNoSearch() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        Tweet tweet = tweetService.createTweet(user1.getUsername(), "@tweet this is a tweet");

        ArrayList<Tweet> tweets = tweetService.getTweets(user1.getUsername(), null);
        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.isTrue(tweets.size() == 1);

        Tweet storedTweet = tweets.get(0);

        Validate.isTrue(tweet.getMessage().equals(storedTweet.getMessage()));
        Validate.isTrue(tweet.getUsername().equals(storedTweet.getUsername()));

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getTweetsNoKeywordsArray() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        Tweet tweet = tweetService.createTweet(user1.getUsername(), "@tweet this is a tweet");

        ArrayList<Tweet> tweets = tweetService.getTweets(user1.getUsername(), null, 0);
        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.isTrue(tweets.size() == 1);

        Tweet storedTweet = tweets.get(0);

        Validate.isTrue(tweet.getMessage().equals(storedTweet.getMessage()));
        Validate.isTrue(tweet.getUsername().equals(storedTweet.getUsername()));
    }
}