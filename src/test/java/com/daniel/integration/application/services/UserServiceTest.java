package com.daniel.integration.application.services;

import com.daniel.application.services.UserService;
import com.daniel.domain.model.user.User;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = com.daniel.context.MinitwitterApplication.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void createNewUser() throws Exception {
        User user = userService.createNewUser("test1", "test1", "test1");
        Validate.notNull(user);
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void followUser() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        User user2 = userService.createNewUser("test2", "test2", "test2");
        userService.followUser(user1.getUsername(), user2.getUsername());
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void unfollowUser() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        User user2 = userService.createNewUser("test2", "test2", "test2");
        userService.followUser(user1.getUsername(), user2.getUsername());
        userService.unfollowUser(user1.getUsername(), user2.getUsername());
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getFollowers() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        User user2 = userService.createNewUser("test2", "test2", "test2");
        userService.followUser(user2.getUsername(), user1.getUsername());

        ArrayList<User> followers = userService.getFollowers(user1.getUsername());
        Validate.notNull(followers);
        Validate.notEmpty(followers);
        Validate.validIndex(followers, 0);
        Validate.isTrue(followers.size() == 1);

        User follower = followers.get(0);
        Validate.isTrue(follower.getUsername().equals(user2.getUsername())
                && follower.getName().equals(user2.getUsername()));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getFollowees() throws Exception {
        User user1 = userService.createNewUser("test1", "test1", "test1");
        User user2 = userService.createNewUser("test2", "test2", "test2");
        userService.followUser(user2.getUsername(), user1.getUsername());

        ArrayList<User> followers = userService.getFollowees(user2.getUsername());
        Validate.notNull(followers);
        Validate.notEmpty(followers);
        Validate.validIndex(followers, 0);
        Validate.isTrue(followers.size() == 1);

        User follower = followers.get(0);
        Validate.isTrue(follower.getUsername().equals(user1.getUsername())
                && follower.getName().equals(user1.getUsername()));
    }
}