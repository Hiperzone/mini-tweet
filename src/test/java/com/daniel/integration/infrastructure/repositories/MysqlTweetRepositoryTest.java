package com.daniel.integration.infrastructure.repositories;

import com.daniel.domain.model.tweet.Tweet;
import com.daniel.domain.model.user.User;
import com.daniel.infrastructure.repositories.MysqlTweetRepository;
import com.daniel.infrastructure.repositories.MysqlUserRepository;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = com.daniel.context.MinitwitterApplication.class)
public class MysqlTweetRepositoryTest {

    @Autowired
    private MysqlUserRepository mysqlUserRepository;

    @Autowired
    private MysqlTweetRepository mysqlTweetRepository;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void store() throws Exception {
        User user = new User("test1", "test1", "test1");
        mysqlUserRepository.store(user);
        Tweet tweet = new Tweet("this is a tweet", user.getUsername());
        mysqlTweetRepository.store(tweet);
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void listTweetsNoSearch() throws Exception {
        User user = new User("test1", "test1", "test1");
        mysqlUserRepository.store(user);
        Tweet tweet = new Tweet("this is a tweet", user.getUsername());
        mysqlTweetRepository.store(tweet);

        ArrayList<Tweet> tweets = (ArrayList<Tweet>) mysqlTweetRepository.listTweets(user, null);
        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.isTrue(tweets.size() == 1);

        Tweet storedTweet = tweets.get(0);

        Validate.isTrue(tweet.getMessage().equals(storedTweet.getMessage()));
        Validate.isTrue(tweet.getUsername().equals(storedTweet.getUsername()));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void listTweetsSingleSearch()  throws Exception {
        User user = new User("test1", "test1", "test1");
        mysqlUserRepository.store(user);
        Tweet tweet = new Tweet("@user this is a tweet with a tag", user.getUsername());
        Tweet tweet2 = new Tweet("this is a tweet", user.getUsername());
        mysqlTweetRepository.store(tweet);
        mysqlTweetRepository.store(tweet2);

        ArrayList<Tweet> tweets = (ArrayList<Tweet>) mysqlTweetRepository.listTweets(user, "@user");
        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.isTrue(tweets.size() == 1);

        Tweet storedTweet = tweets.get(0);

        Validate.isTrue(tweet.getMessage().equals(storedTweet.getMessage()));
        Validate.isTrue(tweet.getUsername().equals(storedTweet.getUsername()));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void listTweetsWithoutKeywordsArray() throws Exception {
        User user = new User("test1", "test1", "test1");
        mysqlUserRepository.store(user);
        Tweet tweet = new Tweet("this is a tweet", user.getUsername());
        mysqlTweetRepository.store(tweet);

        ArrayList<Tweet> tweets = (ArrayList<Tweet>) mysqlTweetRepository.listTweets(
                user, new ArrayList<>(), 0);

        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.isTrue(tweets.size() == 1);

        Tweet storedTweet = tweets.get(0);

        Validate.isTrue(tweet.getMessage().equals(storedTweet.getMessage()));
        Validate.isTrue(tweet.getUsername().equals(storedTweet.getUsername()));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void listTweetsWithKeywordsArray() throws Exception {
        User user = new User("test1", "test1", "test1");
        mysqlUserRepository.store(user);
        Tweet tweet = new Tweet("@user this is a tweet with a tag", user.getUsername());
        Tweet tweet2 = new Tweet("@tweet this is a tweet", user.getUsername());
        mysqlTweetRepository.store(tweet);
        mysqlTweetRepository.store(tweet2);

        ArrayList<String> keywords = new ArrayList<>();
        keywords.add("@user");
        keywords.add("@tweet");

        ArrayList<Tweet> tweets = (ArrayList<Tweet>) mysqlTweetRepository.listTweets(user, keywords, 0);
        Validate.notNull(tweets);
        Validate.notEmpty(tweets);
        Validate.validIndex(tweets, 0);
        Validate.validIndex(tweets, 1);
        Validate.isTrue(tweets.size() == 2);
    }
}