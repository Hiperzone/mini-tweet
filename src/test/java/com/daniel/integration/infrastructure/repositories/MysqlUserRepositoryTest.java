package com.daniel.integration.infrastructure.repositories;

import com.daniel.domain.model.user.User;
import com.daniel.infrastructure.repositories.MysqlUserRepository;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = com.daniel.context.MinitwitterApplication.class)
public class MysqlUserRepositoryTest {

    @Autowired
    private MysqlUserRepository mysqlUserRepository;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void store() throws Exception {
        mysqlUserRepository.store(new User("test1", "test1", "test1"));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void find() throws Exception {
        mysqlUserRepository.store(new User("test1", "test1", "test1"));
        User user = mysqlUserRepository.find("test1");

        Validate.notNull(user.getCreateTime());
        Validate.notNull(user.getName());
        Validate.notNull(user.getUsername());
        Validate.notNull(user.getPassword());

        Validate.notEmpty(user.getName());
        Validate.notEmpty(user.getPassword());
        Validate.notEmpty(user.getUsername());
    }
}