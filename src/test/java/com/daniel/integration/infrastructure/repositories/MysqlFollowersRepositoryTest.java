package com.daniel.integration.infrastructure.repositories;


import com.daniel.domain.model.user.User;
import com.daniel.infrastructure.repositories.MysqlFollowersRepository;
import com.daniel.infrastructure.repositories.MysqlUserRepository;
import org.apache.commons.lang3.Validate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = com.daniel.context.MinitwitterApplication.class)
public class MysqlFollowersRepositoryTest {

    @Autowired
    private MysqlFollowersRepository mysqlFollowersRepository;

    @Autowired
    private MysqlUserRepository mysqlUserRepository;


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void store() throws Exception {
        User follower = new User("test1", "test1", "test1");
        User followee = new User("test2", "test2", "test2");

        mysqlUserRepository.store(follower);
        mysqlUserRepository.store(followee);
        mysqlFollowersRepository.store(follower, followee);

    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void delete() throws Exception {
        User follower = new User("test1", "test1", "test1");
        User followee = new User("test2", "test2", "test2");

        mysqlUserRepository.store(follower);
        mysqlUserRepository.store(followee);
        mysqlFollowersRepository.store(follower, followee);
        mysqlFollowersRepository.delete(follower, followee);

        ArrayList<User> followees = (ArrayList<User>) mysqlFollowersRepository.getFollowees(follower);
        Validate.notNull(followee);
        Assert.assertTrue(followees.size() == 0);

        ArrayList<User> followers = (ArrayList<User>) mysqlFollowersRepository.getFollowers(follower);
        Validate.notNull(followers);
        Assert.assertTrue(followers.size() == 0);
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getFollowers() throws Exception {
        User follower = new User("test1", "test1", "test1");
        User followee = new User("test2", "test2", "test2");

        mysqlUserRepository.store(follower);
        mysqlUserRepository.store(followee);
        mysqlFollowersRepository.store(followee, follower);

        ArrayList<User> followers = (ArrayList<User>) mysqlFollowersRepository.getFollowers(follower);
        Validate.notNull(followers);
        Validate.notEmpty(followers);
        Validate.isTrue(followers.size() == 1);
        Validate.validIndex(followers, 0);

        Validate.isTrue(followee.getUsername().equals(followers.get(0).getUsername()));
    }

    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db.sql")
    @Test
    public void getFollowees() throws Exception {
        User follower = new User("test1", "test1", "test1");
        User followee = new User("test2", "test2", "test2");

        mysqlUserRepository.store(follower);
        mysqlUserRepository.store(followee);
        mysqlFollowersRepository.store(follower, followee);

        ArrayList<User> followees = (ArrayList<User>) mysqlFollowersRepository.getFollowees(follower);
        Validate.notNull(followees);
        Validate.notEmpty(followees);
        Validate.isTrue(followees.size() == 1);
        Validate.validIndex(followees, 0);

        Validate.isTrue(followee.getUsername().equals(followees.get(0).getUsername()));
    }
}
