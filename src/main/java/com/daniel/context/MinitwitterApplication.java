package com.daniel.context;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"com.daniel"})
@SpringBootApplication
public class MinitwitterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinitwitterApplication.class, args);
    }
}
