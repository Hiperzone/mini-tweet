package com.daniel.interfaces.api.controllers;

import com.daniel.application.Constants;
import com.daniel.application.services.TweetService;
import com.daniel.interfaces.api.model.requests.CreateTweetRequest;
import com.daniel.interfaces.api.model.requests.ShowTweetsRequest;
import com.daniel.interfaces.api.model.responses.ShowTweetsResponse;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Controller that receives tweet commands.
 */
@RestController
@RequestMapping(value = Constants.API_ROOT_PATH)
public class TweetController {
    private static final String CREATE_TWEET_PATH = "/tweet";
    private static final String LIST_TWEETS_PATH = "/tweets";

    private TweetService tweetService;

    public TweetController(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    /**
     * Creates an user tweet.
     *
     * @param request   the request containing the message.
     * @param principal the principal with the user's username.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = CREATE_TWEET_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.POST)
    public void createTweet(@Valid @RequestBody CreateTweetRequest request, Principal principal)
            throws DataAccessException {

        tweetService.createTweet(request.getMessage(), principal.getName());
    }

    /**
     * Shows available tweets from the user and its followees.
     * This endpoint is more customizable since it supports paging and multiple keyword search.
     *
     * @param request   the request.
     * @param principal the principal.
     * @return the tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = LIST_TWEETS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ShowTweetsResponse showTweets(@Valid @RequestBody ShowTweetsRequest request, Principal principal)
            throws DataAccessException {

        return new ShowTweetsResponse(tweetService.getTweets(principal.getName(), request.getKeywords(),
                request.getPage()));
    }

    /**
     * Shows available tweets from the user and its followees.
     *
     * @param search    the search keyword.
     * @param principal the principal.
     * @return the tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = LIST_TWEETS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    public ShowTweetsResponse showTweets(@Valid @RequestParam(required = false) String search, Principal principal)
            throws DataAccessException {

        return new ShowTweetsResponse(tweetService.getTweets(principal.getName(), search));
    }
}
