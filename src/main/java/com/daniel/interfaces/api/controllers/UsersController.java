package com.daniel.interfaces.api.controllers;


import com.daniel.application.Constants;
import com.daniel.application.services.CannotFollowYourselfException;
import com.daniel.application.services.CannotUnfollowYourselfException;
import com.daniel.application.services.UserService;
import com.daniel.interfaces.api.model.requests.CreateUserRequest;
import com.daniel.interfaces.api.model.requests.FollowUserRequest;
import com.daniel.interfaces.api.model.requests.UnfollowUserRequest;
import com.daniel.interfaces.api.model.responses.GetFolloweesResponse;
import com.daniel.interfaces.api.model.responses.GetFollowersResponse;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;


/**
 * Controller that receives user commands.
 */
@RestController
@RequestMapping(value = Constants.API_ROOT_PATH)
public class UsersController {

    public static final String API_USERS_PATH = "/user";
    private static final String API_USERS_FOLLOW_PATH = "/user/follow";
    private static final String API_USERS_FOLLOWERS_PATH = "/user/followers";
    private static final String API_USERS_FOLLOWEES_PATH = "/user/followees";

    private UserService userService;

    public UsersController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Creates the given user if it does not exist.
     *
     * @param request the request.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = API_USERS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.POST)
    public void createUser(@RequestBody @Valid CreateUserRequest request) throws DataAccessException {
        userService.createNewUser(request.getName(), request.getUsername(), request.getPassword());
    }

    /**
     * Adds the followee from the list of users that the follower follows.
     * The follower will receieve tweets from the followee.
     *
     * @param request   the request.
     * @param principal the principal.
     * @throws DataAccessException           throw when there is an error during the database transaction.
     * @throws CannotFollowYourselfException throw when the user tries to follow himself.
     */
    @RequestMapping(value = API_USERS_FOLLOW_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.POST)
    public void followUser(@RequestBody @Valid FollowUserRequest request, Principal principal)
            throws DataAccessException, CannotFollowYourselfException {

        userService.followUser(principal.getName(), request.getFollowee());
    }

    /**
     * Removes the followee from the list of users that the follower follows.
     * The follower does not receive more tweets from the followee.
     *
     * @param request   the request.
     * @param principal the principal.
     * @throws DataAccessException             throw when there is an error during the database transaction.
     * @throws CannotUnfollowYourselfException throw when the user tries to un-follow himself.
     */
    @RequestMapping(value = API_USERS_FOLLOW_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.DELETE)
    public void unFollowUser(@RequestBody @Valid UnfollowUserRequest request, Principal principal)
            throws DataAccessException, CannotUnfollowYourselfException {

        userService.unfollowUser(principal.getName(), request.getFollowee());
    }

    /**
     * Returns a list of users that are following the user.
     *
     * @param principal the principal.
     * @return list of followers.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = API_USERS_FOLLOWERS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.GET)
    public GetFollowersResponse getFollowers(Principal principal) throws DataAccessException {
        return new GetFollowersResponse(userService.getFollowers(principal.getName()));
    }

    /**
     * returns the list of users that the user is following.
     *
     * @param principal the principal.
     * @return list of followees.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    @RequestMapping(value = API_USERS_FOLLOWEES_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            method = RequestMethod.GET)
    public GetFolloweesResponse getFollowees(Principal principal) throws DataAccessException {
        return new GetFolloweesResponse(userService.getFollowees(principal.getName()));
    }
}
