package com.daniel.interfaces.api.model.requests;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Represents a request to store a tweet.
 */
public class CreateTweetRequest implements Serializable {

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 140)
    private String message;

    public CreateTweetRequest() {

    }

    public CreateTweetRequest(String message) {
        this.message = message;
    }

    /**
     * Returns the tweet message.
     *
     * @return the tweet message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the tweet message.
     *
     * @param message the tweet message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
