package com.daniel.interfaces.api.model.responses;

import com.daniel.domain.model.user.User;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents a list of followees.
 */
public class GetFolloweesResponse implements Serializable {
    private ArrayList<User> followees;

    public GetFolloweesResponse() {

    }


    public GetFolloweesResponse(ArrayList<User> followees) {
        this.followees = followees;
    }

    /**
     * Returns the list of followees.
     *
     * @return the list of followees.
     */
    public ArrayList<User> getFollowees() {
        return followees;
    }

    /**
     * Sets the list of followees.
     *
     * @param followees the list of followees.
     */
    public void setFollowees(ArrayList<User> followees) {
        this.followees = followees;
    }
}
