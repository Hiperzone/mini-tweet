package com.daniel.interfaces.api.model.requests;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Represents a request to store an user.
 */
public class CreateUserRequest implements Serializable {

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    private String name;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    private String username;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    private String password;

    public CreateUserRequest() {
    }

    public CreateUserRequest(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    /**
     * Returns the user's name.
     *
     * @return the name of the user.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the user's name.
     *
     * @param name the name of the user.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the user's username.
     *
     * @return the username of the user.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the user's username.
     *
     * @param username the username of the user.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the user's password.
     *
     * @return the password of the user.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the user's password.
     *
     * @param password the password of the user.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
