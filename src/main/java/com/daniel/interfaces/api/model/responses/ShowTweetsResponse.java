package com.daniel.interfaces.api.model.responses;

import com.daniel.domain.model.tweet.Tweet;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents the response of a list of tweets.
 */
public class ShowTweetsResponse implements Serializable {

    /**
     * List of tweets.
     */
    private ArrayList<Tweet> tweets;

    public ShowTweetsResponse() {

    }

    public ShowTweetsResponse(ArrayList<Tweet> tweets) {
        this.tweets = tweets;
    }

    /**
     * Returns the list of tweets.
     *
     * @return the list of tweets.
     */
    public ArrayList<Tweet> getTweets() {
        return tweets;
    }

    /**
     * Sets the list of tweets.
     *
     * @param tweets the list of tweets.
     */
    public void setTweets(ArrayList<Tweet> tweets) {
        this.tweets = tweets;
    }
}
