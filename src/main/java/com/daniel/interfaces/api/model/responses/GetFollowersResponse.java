package com.daniel.interfaces.api.model.responses;

import com.daniel.domain.model.user.User;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents a list of followers.
 */
public class GetFollowersResponse implements Serializable {
    private ArrayList<User> followers;

    public GetFollowersResponse() {

    }

    public GetFollowersResponse(ArrayList<User> followers) {
        this.followers = followers;
    }

    /**
     * Returns the list of folllowers.
     *
     * @return the list of followers.
     */
    public ArrayList<User> getFollowers() {
        return followers;
    }

    /**
     * Sets the list of followers.
     *
     * @param followers the list of followers.
     */
    public void setFollowers(ArrayList<User> followers) {
        this.followers = followers;
    }
}
