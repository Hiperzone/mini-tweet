package com.daniel.interfaces.api.model.requests;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Represents a follow request.
 */
public class FollowUserRequest implements Serializable {

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    private String followee;

    public FollowUserRequest() {

    }

    /**
     * Returns the followee.
     *
     * @return the followee.
     */
    public String getFollowee() {
        return followee;
    }

    /**
     * Sets the followee.
     *
     * @param followee the folowee to follow.
     */
    public void setFollowee(String followee) {
        this.followee = followee;
    }
}
