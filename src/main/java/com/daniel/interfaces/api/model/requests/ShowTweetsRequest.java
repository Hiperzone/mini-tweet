package com.daniel.interfaces.api.model.requests;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents the request to obtain a list of tweets.
 * Keywords can be used to limit the scope of the search.
 */
public class ShowTweetsRequest implements Serializable {

    private ArrayList<String> keywords;

    @NotNull
    private Integer page;

    public ShowTweetsRequest() {

    }

    public ShowTweetsRequest(ArrayList<String> keywords, Integer page) {
        this.keywords = keywords;
        this.page = page;
    }

    /**
     * Returns the search keywords.
     *
     * @return the keywords.
     */
    public ArrayList<String> getKeywords() {
        return keywords;
    }

    /**
     * Sets the search keywords.
     *
     * @param keywords the keywords.
     */
    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }

    /**
     * Returns the page number.
     *
     * @return
     */
    public Integer getPage() {
        return page;
    }

    /**
     * Sets the page number.
     *
     * @param page the page number,
     */
    public void setPage(Integer page) {
        this.page = page;
    }
}
