package com.daniel.interfaces.api.model.requests;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UnfollowUserRequest implements Serializable {

    @NotNull
    @NotEmpty
    @Size(min = 1, max = 255)
    private String followee;

    public UnfollowUserRequest() {

    }

    public UnfollowUserRequest(String followee) {
        this.followee = followee;
    }

    /**
     * Returns the followee that the user wants to un-follow.
     *
     * @return the followee.
     */
    public String getFollowee() {
        return followee;
    }

    /**
     * Sets the followee that the user wants to un-follow.
     *
     * @param followee the followee.
     */
    public void setFollowee(String followee) {
        this.followee = followee;
    }
}
