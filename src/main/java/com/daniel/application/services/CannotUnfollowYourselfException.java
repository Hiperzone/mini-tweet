package com.daniel.application.services;


import com.daniel.application.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that is thrown when the user tries to un-follow himself.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = Constants.Strings.CANNOT_UNFOLLOW_YOURSELF)
public class CannotUnfollowYourselfException extends Exception {

    public CannotUnfollowYourselfException(String message) {
        super(message);
    }
}
