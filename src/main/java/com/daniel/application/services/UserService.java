package com.daniel.application.services;


import com.daniel.application.Constants;
import com.daniel.domain.model.follower.FollowerRepository;
import com.daniel.domain.model.user.User;
import com.daniel.domain.model.user.UserRepository;
import com.daniel.infrastructure.repositories.MysqlFollowersRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService {
    public static final String USERNAME_REGEX = "([aA-zZ0-9_])+";
    private UserRepository userRepository;
    private FollowerRepository followersRepository;

    public UserService(UserRepository userRepository, MysqlFollowersRepository mysqlFollowersRepository) {
        this.userRepository = userRepository;
        this.followersRepository = mysqlFollowersRepository;
    }

    /**
     * Creates a new user.
     * Passwords are hashed with SHA256.
     *
     * @param name     the name of the user.
     * @param username the username of the user.
     * @param password the plaintext or hashed password.
     * @return the user.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public User createNewUser(String name, String username, String password) throws DataAccessException {
        Validate.matchesPattern(username, USERNAME_REGEX);
        User user = new User(name, username, password);

        //Encode the password using SHA256
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
        user.setPassword(shaPasswordEncoder.encodePassword(password, null));

        userRepository.store(user);
        return user;
    }

    /**
     * Allows an user to follow another user.
     *
     * @param follower the follower.
     * @param followee the followee.
     * @throws DataAccessException           throw when there is an error during the database transaction.
     * @throws CannotFollowYourselfException throw when the user is trying to follow himself.
     */
    public void followUser(String follower, String followee) throws DataAccessException,
            CannotFollowYourselfException {

        User user = userRepository.find(follower);
        User userToFollow = userRepository.find(followee);

        if (user.sameAs(userToFollow)) {
            throw new CannotFollowYourselfException(Constants.Strings.CANNOT_FOLLOW_YOURSELF);
        }

        followersRepository.store(user, userToFollow);
    }

    /**
     * Allows an user to un-follow another user.
     *
     * @param follower the follower.
     * @param followee the followee.
     * @throws DataAccessException             throw when there is an error during the database transaction.
     * @throws CannotUnfollowYourselfException thrown when the user is trying to un-follow himself.
     */
    public void unfollowUser(String follower, String followee)
            throws DataAccessException, CannotUnfollowYourselfException {

        User user = userRepository.find(follower);
        User userToFollow = userRepository.find(followee);

        if (user.sameAs(userToFollow)) {
            throw new CannotUnfollowYourselfException(Constants.Strings.CANNOT_UNFOLLOW_YOURSELF);
        }

        followersRepository.delete(user, userToFollow);
    }

    /**
     * Returns the list of followers that are folllowing the user.
     *
     * @param username the user's username.
     * @return list of followers.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public ArrayList<User> getFollowers(String username) throws DataAccessException {
        User user = userRepository.find(username);
        return (ArrayList<User>) followersRepository.getFollowers(user);
    }

    /**
     * Returns the list of followees that the user is following.
     *
     * @param username the user's username.
     * @return list of followees.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public ArrayList<User> getFollowees(String username) throws DataAccessException {
        User user = userRepository.find(username);
        return (ArrayList<User>) followersRepository.getFollowees(user);
    }
}
