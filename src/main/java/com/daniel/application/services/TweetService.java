package com.daniel.application.services;

import com.daniel.domain.model.tweet.Tweet;
import com.daniel.domain.model.tweet.TweetRepository;
import com.daniel.domain.model.user.User;
import com.daniel.domain.model.user.UserRepository;
import com.daniel.infrastructure.repositories.MysqlTweetRepository;
import com.daniel.infrastructure.repositories.MysqlUserRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Service that handles the creation and fetching of tweets.
 */
@Service
public class TweetService {
    private UserRepository userRepository;
    private TweetRepository tweetRepository;

    public TweetService(MysqlUserRepository mysqlUserRepository, MysqlTweetRepository mysqlTweetRepository) {
        userRepository = mysqlUserRepository;
        tweetRepository = mysqlTweetRepository;
    }

    /**
     * Stores an user's tweet.
     *
     * @param username the username.
     * @param message  the message.
     * @return the tweet.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public Tweet createTweet(String username, String message) throws DataAccessException {
        Tweet tweet = new Tweet(message, username);

        Validate.notNull(tweet);

        userRepository.find(tweet.getUsername());
        tweetRepository.store(tweet);

        return tweet;
    }

    /**
     * Returns tweets posted by the user and by its followers.
     * This method uses paging to reduce load on the database.
     *
     * @param username the user's username.
     * @param keywords optional keyword search.
     * @param page     the page number.
     * @return list of tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public ArrayList<Tweet> getTweets(String username, ArrayList<String> keywords, int page)
            throws DataAccessException {
        Validate.isTrue(page >= 0);

        User user = userRepository.find(username);
        return (ArrayList<Tweet>) tweetRepository.listTweets(user, keywords, page);
    }

    /**
     * Returns tweets posted by the user and by its followers.
     * This method uses paging to reduce load on the database.
     *
     * @param username the user's username.
     * @param search   optional keyword search.
     * @return list of tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    public ArrayList<Tweet> getTweets(String username, String search) throws DataAccessException {
        User user = userRepository.find(username);
        return (ArrayList<Tweet>) tweetRepository.listTweets(user, search);
    }
}
