package com.daniel.application.services;


import com.daniel.application.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that is thrown when the user tries to follow himself.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = Constants.Strings.CANNOT_FOLLOW_YOURSELF)
public class CannotFollowYourselfException extends Exception {

    public CannotFollowYourselfException(String message) {
        super(message);
    }
}
