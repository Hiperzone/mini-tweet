package com.daniel.application;

public abstract class Constants {

    public static final String API_ROOT_PATH = "/api";
    public static final int TWEETS_PER_PAGE = 10;

    public static final String REALM = "MINITWEET";
    public static final String ROLE_USER = "USER";
    public static final String ROOT_PATH_ALL = "/**";
    public static final String API_PATH_ALL = "/api/**";

    public abstract static class Strings  {
        public static final String CANNOT_FOLLOW_YOURSELF = "You cannot follow yourself";
        public static final String CANNOT_UNFOLLOW_YOURSELF = "You cannot un-follow yourself";
    }
}
