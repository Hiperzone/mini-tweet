package com.daniel.infrastructure.repositories;

import com.daniel.application.Constants;
import com.daniel.domain.model.tweet.Tweet;
import com.daniel.domain.model.tweet.TweetRepository;
import com.daniel.domain.model.user.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MysqlTweetRepository implements TweetRepository {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public MysqlTweetRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void store(Tweet tweet) throws DataAccessException {
        String query = "INSERT INTO tweets (tweet_message, user_username) VALUES (:message, :username)";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("username", tweet.getUsername());
        namedParameters.put("message", tweet.getMessage());
        namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public List<Tweet> listTweets(User user, ArrayList<String> searchKeywords, int page) throws DataAccessException {
        String keywordPartialQuery = buildKeywordSearch(searchKeywords);
        String query = ("SELECT * FROM " +
                "  (SELECT " +
                "    tweet_message, " +
                "    tweet_creation_time, " +
                "    user_username AS posted_by " +
                "  FROM tweets AS q1 ") +
                "  WHERE (user_username = :username " +
                keywordPartialQuery +
                ")) AS q1 " +
                "  UNION " +
                "    (SELECT " +
                "      tweet_message, " +
                "      tweet_creation_time, " +
                "      user_username AS posted_by " +
                "    FROM tweets INNER JOIN followers " +
                "    WHERE (followers.follower = :username AND tweets.user_username = followers.followee " +
                keywordPartialQuery +
                ")) ORDER BY tweet_creation_time DESC LIMIT " +
                page * Constants.TWEETS_PER_PAGE +
                ", " +
                Constants.TWEETS_PER_PAGE;

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("username", user.getUsername());
        return namedParameterJdbcTemplate.query(query, namedParameters, new TweetMapper());
    }

    @Override
    public List<Tweet> listTweets(User user, String searchKeyword) throws DataAccessException {
        String keywordPartialQuery = buildKeywordSearch(searchKeyword);

        String query = ("SELECT * FROM " +
                "  (SELECT " +
                "    tweet_message, " +
                "    tweet_creation_time, " +
                "    user_username AS posted_by " +
                "  FROM tweets AS q1 ") +
                "  WHERE (user_username = :username " +
                keywordPartialQuery +
                ")) AS q1 " +
                "  UNION " +
                "    (SELECT " +
                "      tweet_message, " +
                "      tweet_creation_time, " +
                "      user_username AS posted_by " +
                "    FROM tweets INNER JOIN followers " +
                "    WHERE (followers.follower = :username AND tweets.user_username = followers.followee " +
                keywordPartialQuery +
                ")) ORDER BY tweet_creation_time DESC";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("username", user.getUsername());
        return namedParameterJdbcTemplate.query(query, namedParameters, new TweetMapper());
    }

    /**
     * Builds the search query for a single search keyword.
     *
     * @param searchKeyword the keyword.
     * @return a string with the partial sql query that includes the keywords to search for.
     */
    private String buildKeywordSearch(String searchKeyword) {
        if (searchKeyword == null) {
            return "";
        }

        ArrayList<String> keywords = new ArrayList<>();
        keywords.add(searchKeyword);
        return buildKeywordSearch(keywords);
    }

    /**
     * Builds the search query for all the keywords in the array.
     *
     * @param keywords the array of keywords.
     * @return a string with the partial sql query that includes the keywords to search for.
     */
    private String buildKeywordSearch(ArrayList<String> keywords) {
        if (keywords == null || keywords.size() == 0) {
            return "";
        }

        StringBuilder keywordBuilder = new StringBuilder();
        keywordBuilder.append("AND (");

        for (int i = 0; i < keywords.size(); i++) {
            keywordBuilder.append("tweet_message LIKE '%")
                    .append(keywords.get(i));

            if (i == (keywords.size() - 1)) {
                keywordBuilder.append("%' ");
            } else {
                keywordBuilder.append("%' OR ");
            }
        }

        keywordBuilder.append(")");

        return keywordBuilder.toString();
    }

    /**
     * Class that maps the database tweets table.
     */
    public static class TweetMapper implements RowMapper<Tweet> {

        @Override
        public Tweet mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Tweet(rs.getString("tweet_message"),
                    rs.getTimestamp("tweet_creation_time"),
                    rs.getString("posted_by"));
        }
    }
}
