package com.daniel.infrastructure.repositories;

import com.daniel.domain.model.user.User;
import com.daniel.domain.model.user.UserRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class MysqlUserRepository implements UserRepository {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public MysqlUserRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void store(User user) throws DataAccessException {
        String sql = "INSERT INTO user (user_username, user_password, user_name) VALUES (:username, :password, :name)";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("username", user.getUsername());
        namedParameters.put("password", user.getPassword());
        namedParameters.put("name", user.getName());
        namedParameterJdbcTemplate.update(sql, namedParameters);

        String authorityQuery = "INSERT INTO authorities (username) VALUES (:username)";
        namedParameterJdbcTemplate.update(authorityQuery, namedParameters);
    }

    @Override
    public User find(String username) throws DataAccessException {
        String sql = "SELECT * FROM user WHERE (user_username = :username)";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("username", username);
        return namedParameterJdbcTemplate.queryForObject(sql, namedParameters, new UserMapper());
    }

    /**
     * Class that maps the database user table the User Entity object.
     */
    public static class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(rs.getString("user_name"), rs.getString("user_username"),
                    rs.getString("user_password"), rs.getDate("user_create_time"));
        }
    }
}
