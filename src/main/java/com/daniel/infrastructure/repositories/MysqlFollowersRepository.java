package com.daniel.infrastructure.repositories;


import com.daniel.domain.model.follower.FollowerRepository;
import com.daniel.domain.model.user.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MysqlFollowersRepository implements FollowerRepository {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public MysqlFollowersRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void store(User follower, User followee) throws DataAccessException {
        String sql = "INSERT INTO followers (follower, followee) VALUES (:follower, :followee)";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("follower", follower.getUsername());
        namedParameters.put("followee", followee.getUsername());
        namedParameterJdbcTemplate.update(sql, namedParameters);
    }

    @Override
    public void delete(User follower, User followee) throws DataAccessException {
        String sql = "DELETE FROM followers WHERE (follower = :follower AND followee = :followee)";
        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("follower", follower.getUsername());
        namedParameters.put("followee", followee.getUsername());
        namedParameterJdbcTemplate.update(sql, namedParameters);
    }

    @Override
    public List<User> getFollowers(User followee) throws DataAccessException {
        String query = "SELECT * FROM followers INNER JOIN user WHERE (" +
                "user.user_username = followers.follower AND followee = :followee)";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("followee", followee.getUsername());
        return namedParameterJdbcTemplate.query(query, namedParameters, new FollowerMapper());
    }

    @Override
    public List<User> getFollowees(User follower) throws DataAccessException {
        String query = "SELECT * FROM followers INNER JOIN user WHERE (" +
                "user.user_username = followers.followee AND follower = :follower)";

        Map<String, String> namedParameters = new HashMap<>();
        namedParameters.put("follower", follower.getUsername());
        return namedParameterJdbcTemplate.query(query, namedParameters, new FolloweeMapper());
    }

    /**
     * Class that maps the database followers table followees field.
     */
    public static class FolloweeMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(rs.getString("user_name"), rs.getString("user_username"),
                    rs.getString("user_password"), rs.getDate("user_create_time"));
        }
    }

    /**
     * Class that maps the database followers table followers field.
     */
    public static class FollowerMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(rs.getString("user_name"), rs.getString("user_username"),
                    rs.getString("user_password"), rs.getDate("user_create_time"));
        }
    }
}
