package com.daniel.domain.model.tweet;


import com.daniel.domain.model.user.User;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.List;

/**
 * Tweets repository interface.
 */
public interface TweetRepository {

    /**
     * Creates a new entry in the tweets table.
     *
     * @param tweet the tweet.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    void store(Tweet tweet) throws DataAccessException;

    /**
     * Lists available tweets from the user.
     * The list can be pageable.
     *
     * @param user           the user.
     * @param searchKeywords keywords to search for.
     * @param page           the page number.
     * @return list of tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    List<Tweet> listTweets(User user, ArrayList<String> searchKeywords, int page) throws DataAccessException;

    /**
     * Lists available tweets from the user.
     * The list can be pageable.
     *
     * @param user   the user.
     * @param searchKeyword the keyword to search for.
     * @return list of tweets.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    List<Tweet> listTweets(User user, String searchKeyword) throws DataAccessException;
}
