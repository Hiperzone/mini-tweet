package com.daniel.domain.model.tweet;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Represents a tweet from a user.
 */
public class Tweet implements Serializable {
    @NotEmpty
    @NotNull
    @Size(min = 1, max = 140)
    private String message;

    @NotNull
    private Timestamp creationTime;

    @NotEmpty
    @NotNull
    @Size(min = 1, max = 255)
    private String username;

    public Tweet(String message, String username) {
        this.message = message;
        this.username = username;
    }

    public Tweet(String message, Timestamp creationTime, String username) {
        this.message = message;
        this.creationTime = creationTime;
        this.username = username;
    }

    /**
     * Returns the message that the user posted.
     *
     * @return the message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the message/tweet.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * The date when the tweet was posted.
     *
     * @return the creation date.
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the tweet post date.
     *
     * @param creationTime the creation date.
     */
    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * Returns the user's username that posted.
     *
     * @return the username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the user's username that posted the tweet.
     *
     * @param username the username that posted the tweet.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Tweet tweet = (Tweet) o;

        return new EqualsBuilder()
                .append(message, tweet.message)
                .append(creationTime, tweet.creationTime)
                .append(username, tweet.username)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(message)
                .append(creationTime)
                .append(username)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "message='" + message + '\'' +
                ", creationTime=" + creationTime +
                ", username='" + username + '\'' +
                '}';
    }
}
