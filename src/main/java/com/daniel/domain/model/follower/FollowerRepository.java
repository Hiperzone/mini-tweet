package com.daniel.domain.model.follower;

import com.daniel.domain.model.user.User;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Follower repository interface.
 */
public interface FollowerRepository {

    /**
     * Creates a new entry in the followers table, allowing one user to follow another user's tweets.
     *
     * @param follower the follower.
     * @param followee the followee.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    void store(User follower, User followee) throws DataAccessException;

    /**
     * Deletes an entry from the followers table, allowing one user to unfollow another user's tweets.
     *
     * @param follower the follower.
     * @param followee the followee.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    void delete(User follower, User followee) throws DataAccessException;


    /**
     * Returns a list of users that are following the user.
     *
     * @param followee the followee.
     * @return the list of followers.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    List<User> getFollowers(User followee) throws DataAccessException;

    /**
     * Returns a list of users that the user is following.
     *
     * @param follower the follower.
     * @return the list of followees.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    List<User> getFollowees(User follower) throws DataAccessException;
}
