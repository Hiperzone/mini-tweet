package com.daniel.domain.model.user;


import com.daniel.domain.shared.Entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Represents an user entity.
 * The user is root entity of the domain.
 * Users may post tweets, follow and un-follow users at will.
 */
public final class User implements Entity<User>, Serializable {

    @NotEmpty
    @NotNull
    @Size(min = 1, max = 255)
    private String name;

    @NotEmpty
    @NotNull
    @Size(min = 1, max = 255)
    private String username;

    @NotEmpty
    @NotNull
    @JsonIgnore
    private String password;

    @JsonIgnore
    @NotNull
    private Date createTime;

    public User() {

    }

    public User(String name, String username, String password) {
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public User(String name, String username, String password, Date createTime) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.createTime = createTime;
    }

    /**
     * Returns the user's username.
     *
     * @return the user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the user's username.
     *
     * @param username the user's username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the user's password.
     *
     * @return the user's password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the user's password.
     *
     * @param password the user's password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the user's creation date.
     *
     * @return the user's creation date.
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * Sets the user's creation date.
     *
     * @param createTime the user's creation date.
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * Returns the user's name.
     *
     * @return the user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the user's name.
     *
     * @param name the user's name.
     */
    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean sameAs(User other) {
        Validate.notNull(other);
        return username.equals(other.getUsername());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return new EqualsBuilder()
                .append(name, user.name)
                .append(username, user.username)
                .append(password, user.password)
                .append(createTime, user.createTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(username)
                .append(password)
                .append(createTime)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
