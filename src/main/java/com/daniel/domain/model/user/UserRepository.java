package com.daniel.domain.model.user;

import org.springframework.dao.DataAccessException;

/**
 * User repository interface.
 */
public interface UserRepository {

    /**
     * Creates a user.
     * Passwords use SHA256
     *
     * @param user the user to store.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    void store(User user) throws DataAccessException;


    /**
     * Searches for a specific user in the database.
     *
     * @param username the username to find.
     * @return the user if it exists.
     * @throws DataAccessException throw when there is an error during the database transaction.
     */
    User find(String username) throws DataAccessException;
}
