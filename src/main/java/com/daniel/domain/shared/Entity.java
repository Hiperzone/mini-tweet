package com.daniel.domain.shared;

/**
 * Representation of a domain entity.
 *
 * @param <T> domain entity object.
 */
public interface Entity<T> {

    /**
     * Compares if the entity is equal to another entity.
     *
     * @param other the other entity
     * @return true if they are equal.
     */
    boolean sameAs(T other);
}
